<?php
	//removing item from the shopping list if it's in the shopping cart
	require_once('settings.php');
	$db = new PDO('mysql:host=localhost;dbname='.$dbname, $dbuser, $dbpass);
	session_start();

	$user = $_SESSION["user"];
	$g = $db->prepare('SELECT * FROM grocerylist WHERE user=:user');
	$g->execute(array(':user'=>$user));

	//searching all items in cart and deleting them
	foreach ($g->fetchAll() as $key => $value) {
		if($value['incart'] == true){
			$r = $db->prepare('DELETE FROM grocerylist WHERE id='.$value['id']);
			$r->execute();
		}
	}
?>
