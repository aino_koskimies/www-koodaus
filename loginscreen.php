<?php
  require_once('settings.php');
//logging in (starting session), if user types correct username and password
if (isset($_POST['user']) && isset($_POST['passw'])) {
  $db = new PDO('mysql:host=localhost;dbname='.$dbname, $dbuser, $dbpass);
  $user = $_POST['user'];

  //get password from database using username
  $stm = $db->prepare('SELECT passwordhash, id, user FROM users WHERE user=:user');
  $stm->execute(array(':user'=>$user));

  $r = $stm->fetchAll(PDO::FETCH_ASSOC);

  //if password mantces one in the database, log the user in
  if (password_verify($_POST['passw'], $r[0]['passwordhash'])) {
    //logging in
    session_start();
    $_SESSION['id'] = $r[0]['id'];
    $_SESSION['user'] = $r[0]['user'];
    header('Location: '.$redirect);
  }
  else {
    //login failed
    header('Location: '.$redirect);
  }
}
 ?>

<h1 class="header">Kauppalappu</h1>
<p>kirjaudu nähdäksesi oman kauppalappusi</p>
</br>
<form name='loginForm' action="loginscreen.php" method="post">
  <div class='buttoncage'>
    <input type="text" name="user" class="logForm" placeholder="käyttäjätunnus"/>
    <input type="password" name="passw" class="logForm" placeholder="salasana"/>
  </div>
  </br>
  <input type='submit' class='button actionbutton' value='Kirjaudu' id='loginbutton'>
</form>
<div class="button actionbutton" id="regbutton"><a href="index.php?p=register">Rekisteröidy</a></div></div>
