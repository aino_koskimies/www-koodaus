-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 25, 2017 at 09:29 PM
-- Server version: 5.7.17-0ubuntu0.16.10.1
-- PHP Version: 7.0.8-3ubuntu3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grocerylist`
--

-- --------------------------------------------------------

--
-- Table structure for table `grocerylist`
--

CREATE TABLE `grocerylist` (
  `id` int(11) NOT NULL,
  `incart` tinyint(1) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `user` varchar(50) NOT NULL COMMENT 'foreign key'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grocerylist`
--

INSERT INTO `grocerylist` (`id`, `incart`, `amount`, `item`, `user`) VALUES
(52, 0, 1, 'muuttaminen klikkaamalla', 'aino'),
(53, 0, 1, 'teemat', 'aino'),
(57, 0, 3, 'naksua', 'kissa'),
(60, 0, 1, 'kuut', 'kuutti'),
(61, 0, 2, 'kuut', 'kuutti'),
(62, 0, 3, 'kuut', 'kuutti'),
(63, 0, 1, 'kÃ¤ytÃ¤ id:tÃ¤ kÃ¤yttÃ¤jÃ¤n identifioimisessa', 'aino'),
(64, 0, 1, 'silakka', 'testikuutti'),
(65, 0, 3, 'lohta', 'testikuutti');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `passwordhash` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `passwordhash`, `location`, `user`) VALUES
(1, '$2y$10$Ttq0VHlyUD3UsaBRhQJ9X.cvD9.SsITXMJDxM8Fd/q8tYtWbIJDbu', 'Tampere', 'aino'),
(2, '$2y$10$N5kkl2clLYW1tp3C9766YOgCxhrgsVIrEhmkZR8jFtibPrqmnHkBm', 'Helsinki', 'kissa'),
(3, '$2y$10$tv9EK/Qe6NVtnJELhedLgOCle83/leVNxBIRaOGXZspiBV47UJjx.', 'Kouvola', 'kuutti'),
(4, '$2y$10$70rFYWMOXPS9eOSvZhH/kukCst2hJ59g/lxZGP1migRBrJW9iFazG', 'Lappeenranta', 'testikuutti');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grocerylist`
--
ALTER TABLE `grocerylist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grocerylist`
--
ALTER TABLE `grocerylist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
