<?php
  //adding new item and its amount to the shopping list
  session_start();
  require_once('settings.php');
  $db = new PDO('mysql:host=localhost;dbname='.$dbname, $dbuser, $dbpass);

   if(isset($_POST['glItem'])&&($_POST['glItem']!='')){
    $klitem = $_POST['glItem'];
    $qty = $_POST['itemQty'];
    $user = $_SESSION['user'];

    $s = $db->prepare('INSERT INTO grocerylist(item, amount, user) VALUES(:item, :amount, :user)');
    $s->execute(array(':item' => $klitem, ':amount' => $qty, ':user' => $user));
  }
?>
