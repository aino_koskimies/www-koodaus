<?php
  //logging user out by destroying the session
  session_start();
  session_destroy();
  header('Location: '.$redirect);
?>
