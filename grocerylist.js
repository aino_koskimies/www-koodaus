var inprogress = false; //preventing collations of ajax operations

$(document).ready(function(){
  //adding item to the list
  $('#addbutton').click(function(){
    if(inprogress == false){
      inprogress = true;
      var toAdd = $('input[name=glItem]').val();
      var itemQty = $('input[name=qty]').val();
      if (toAdd != '') {
        //checking that qty input is in correct form
        if ((itemQty >= 1 && $.isNumeric(itemQty) == true)) {
          //writing only numbers greater than 1 on the list
          if(itemQty == 1){
            $('.list').append('<li class="item"><div class="button chgbutton">&#9998</div><div class="itemtext">' + toAdd + '</div></li>');
            $('input').val('');
          }
          else {
            $('.list').append('<li class="item"><div class="button chgbutton">&#9998</div><div class="itemtext">' + itemQty + ' ' + toAdd + '</div></li>');
            $('input').val('');
          }

          var request = $.ajax({
            url: "listadd.php",
            type: "POST",
            data: {
              "glItem": toAdd,
              "itemQty": itemQty
            },
            dataType: "html",
            success: function () {
              console.log('item in database');
              inprogress = false;
            }
          });
        }
        else {
          //show error note if user input is not correct
          $('#errorfield').append('<p class="error"></p>');
          $('.error').text('Tarkista syöte').fadeIn();
          $('.error').fadeOut(2500, function(){
            $(this).remove();
          });
          inprogress = false;
        }
      }
    }
  });

  //adding item to the cart (over stroking)
  $(document).on('click','.itemtext',function(){
    if (inprogress == false){
      inprogress = true;
      $(this).toggleClass('stroked');
      var itemid = $(this).parent().attr("id");
      console.log("id " + itemid);
      var request = $.ajax({
      	url: "itemincart.php",
      	type: "POST",
      	data: {
      		"itemID": itemid
      	},
      	dataType: "html",
        success: function () {
          console.log('item in cart');
          inprogress = false;
        }
      });
    }
  });

  //removing stroked items from the list and the database
  $('#rmbutton').click(function(){
    if (inprogress == false){
      inprogress = true;

      var request = $.ajax({
        url: "listremove.php",
        success: function () {
          console.log("items removed");
          $('.stroked').parent().remove();
          inprogress = false;
        },
        error: function () {
          console.log("error while removing item");
        },
      });
    }
  });

  $(document).on('click', '.chgbutton', function(){
    //moving itemtext to inputfield for changing the text
    var numberChange = "";

    var findItem = $(this).parent().children()[1];
    var itemChange = findItem.innerText;
    findItem.remove();

    if ($.isNumeric(itemChange.charAt(0)) == false){
      numberChange = 1;
    }
    else{
      for (var i=0; $.isNumeric(itemChange.charAt(i)) == true; i++){
        numberChange = numberChange + itemChange.charAt(i);
        //removing number from item
        itemChange = itemChange.substring(1);

      }
      //removing space in front of items name
      itemChange = itemChange.substring(1);
    }

    //adding fields with current amount and item
    $(this).parent().append('<input class="quantity" type="number" min="1" value="' + numberChange + '"/><input class="listInput type="text" value="' + itemChange + '"/>');


    //getting changed text and amount

    //updating changes to the database

  });
});
