<div id='errorfield'></div>
  <h1>Kauppalappu</h1>
  <form name="glForm">
    <div class="buttoncage">
      <input type="number" name="qty" min="1" class="quantity" value="1"/>
      <input type="text" name="glItem" class="glInput"/>
    </div>
    <br />
    <div class="button actionbutton" id="addbutton">Lisää</div>

  </form>
  <br />
  <ul class='list'>

    <?php
    //getting items from database
    $db = new PDO('mysql:host=localhost;dbname='.$dbname, $dbuser, $dbpass);
    session_start();

    $user = $_SESSION['user'];
    $g = $db->prepare('SELECT * FROM grocerylist WHERE user=:user');
    $g->execute(array(':user'=>$user));

    foreach ($g->fetchAll() as $key => $value) {
    	//writing amount only if it's greater than 1
      if ($value['amount'] == 1) {
        $qty = '';
      }
      else {
        $qty = $value['amount']." ";
      }
      //writing fetched item and marking if it's in cart
    	if ($value['incart'] == false) {
      	echo "<li class='item' id='id_".$value['id']."'><div class='button chgbutton'>&#9998</div><div class='itemtext'>".$qty.$value['item']."</div></li>";
      }
      else {
      	echo "<li class='item' id='id_".$value['id']."'><div class='button chgbutton'>&#9998</div><div class='itemtext stroked'>".$qty.$value['item']."</div></li>";
      }
    }
    ?>
  </ul>
  <br />
  <div class="button actionbutton" id="rmbutton">Poista yliviivatut</div>
<div class="weatherbox">
<?php include_once('weather.php'); ?>
</div>
