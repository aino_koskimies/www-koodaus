<?php
  //getting weather information from OpenWeatherMap.org
  $db = new PDO('mysql:host=localhost;dbname='.$dbname, $dbuser, $dbpass);

  $stm = $db->prepare('SELECT location FROM users WHERE user=:user');
    $stm->execute(array(':user'=>$_SESSION['user']));

    $r = $stm->fetchAll(PDO::FETCH_ASSOC);
    //searching users location from the database
    if (count($r) === 1) {
      $location = $r[0]['location'];
    }
    else {
      //finding location from database failed
      print('Paikkakunnan hakeminen epäonnistui');
    }

  //finding weather based on users location
  $json = file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$location.',fi&appid=39c58348d0038aed4eadbed3d54d801b');
  $obj = json_decode($json);
  $temp = round((($obj->main->temp)-273.15), 1);
  print('Sää paikkakunnallasi: '.$location.'</br>'.$obj->weather[0]->description.'</br>');
  print('Lämmintä: '.$temp.'°c');
?>
