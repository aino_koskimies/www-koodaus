<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="grocerylist.css"/>
  <meta charset="utf-8"/>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

  <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="grocerylist.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!--for responsibility-->
  <title>Kauppalappu</title>
</head>

<body>

<?php
  session_start();
  require_once('settings.php');
  if ($_SESSION['id'] != null) {
    print('<div class="loginfield"><p id="userdata">Kirjautuneena: '.$_SESSION['user'].'</p>');
    print('<div class="button" id="logbutton"><a href="logout.php">Kirjaudu ulos</a></div></div>');

    require_once('grocerylisthtml.php');
  }
  elseif ($_GET['p'] == 'register') {
    require_once('register.php');
  }
  else {
    require_once('loginscreen.php');
  }
?>

  <p class='centered'>Developed by Aino Koskimies, last changed Jan 2017</p>
</body>
</html>
