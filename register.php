<?php
require_once('settings.php');
//registering and starting session if registering is success
if(isset($_POST['user']) && isset($_POST['passw']) && isset($_POST['passwagain']) && isset($_POST['loc'])) {
  $db = new PDO('mysql:host=localhost;dbname='.$dbname, $dbuser, $dbpass);

  $user = $_POST['user'];
  //checking that username is available
  $stm = $db->prepare('SELECT user FROM users WHERE user=:user');
  $stm->execute(array(':user'=>$user));

  $found = $stm->fetchAll(PDO::FETCH_ASSOC);

  if (empty($found) == false){
    //if username is already at the database
    //$r[0]['user']
    //error message print('<p class="error">Käyttäjätunnus on jo käytössä</p>');
    header('Location: '.$redirect.'?p=register');
  }
  elseif($_POST['passw'] != $_POST['passwagain']){
    //if passwords don't match
    //error message print('<p class="error">Salasanat eivät täsmää</p>');
    header('Location: '.$redirect.'?p=register');
  }
  else {
    //save new user and hashed password to the database
    $hashed = password_hash($_POST['passw'], PASSWORD_BCRYPT);
    $s = $db->prepare('INSERT INTO users(user,passwordhash, location) VALUES(:user, :passwordhash, :location)');
    $s->execute(array(':user' => $_POST['user'], ':passwordhash' => $hashed, ':location' => $_POST['loc']));
    /*NOTe: SUCCESS*/
    //log the new user in
    session_start();
    $_SESSION['id'] = $r[0]['id'];
    $_SESSION['user'] = $r[0]['user'];
    header('Location: '.$redirect); //WHY THIS DOESN'T WORK?
  }
}
 ?>

<h1 class="header">Kauppalappu</h1>
<p>rekisteröidy luodaksesi oman kauppalappusi</p>
</br>
<form name='registerForm' action="register.php" method="post">
  <div class='buttoncage register'>
    <input type='text' name='user' class='regForm' placeholder='käyttäjätunnus'/>
    <input type='password' name='passw' class='regForm' placeholder='salasana'/>
    <input type='password' name='passwagain' class='regForm' placeholder='salasana uudestaan'/>
    <input type='text' name='loc' class='regForm' placeholder='paikkakunta'/>
  </div>
  </br>
  <input type='submit' class='button actionbutton' value='Rekisteröidy' id='registerbutton'/>
</form>
