# README #

This document provided only in Finnish

### What is this repository for? ###

Tämä repository sisältää koodin kauppalapuksi tarkoitettuun sovellukseen
Repository päivittyy jatkuvasti kun uusia ominaisuuksia lisätään sovellukseen
Aluksi kauppalappu oli kurssin lopputyö, mutta se jatkoi ja jatkaa kehittymistään vielä pitkään, sillä projektin aihe osoittautui varsin hyödylliseksi käytännössä ja jatkokehitysideoita on runsaasti jäljellä.

### How to run the code ###

-toimivin versio osoitteessa www.gpiste.org/kuuttilappu/index.php
testikäyttäjä: käyttäjätunnus: testikuutti, salasana: silakkapihvi

TAI

-lataa tiedostot/kloonaa repository
-sijoita tiedostot palvelimellehakemistoon “wwwroot”/aino/www-ht/
-luo tietokanta grocerylist
-luo tietokannan käyttäjä aino sille salasana aino ja anna sille täydet oikeudet yllä olevaan tietokantaan
-importaa tiedosto grocerylist.sql luotuun tietokantaan
-suuntaa selain osoitteeseen localhost/aino/www-ht/index.php ja rekisteröidy sisään sovellukseen tai käytä testikäyttäjää: käyttäjätunnus: testikuutti, salasana: silakkapihvi

### Some of the development ideas ###

-viivakoodin skannaus
-tilasto ostetuista tuotteista (kaikki käyttäjät tai yksittäiset käyttäjät)
-tilasto siitä, mitä on kaapissa -> reseptihaku
